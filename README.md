# calibrations

Dépôt git contenant des données de calibration de la ligne DISCO et leur fichiers 
de traitement.

## Flux 

Mesure du Flux sur la ligne avec un photo-diode

### Evolution 

Sur TELEMOS 

Au 10x
![Telfluxevo10](./Flux/figures/sumup/Comparaison_flux_at_sample_TELEMOS_(10x).png)
![Telfluxevo10n](./Flux/figures/sumup/Comparaison_normed_flux_at_sample_TELEMOS_(10x).png)

Au 40x
![Telfluxevo10](./Flux/figures/sumup/Comparaison_flux_at_sample_TELEMOS_(40x).png)
![Telfluxevo10n](./Flux/figures/sumup/Comparaison_normed_flux_at_sample_TELEMOS_(40x).png)

### Septembre 2023

Avant Polypheme
![Polflux](./Flux/figures/Flux_Polypheme_14092023.png)

Niveau de l'échantillon sur Polypheme 40x 
![Polfluxsample](./Flux/figures/Flux_Polypheme_14092023_sample_x40.png)

Niveau de l'échangillon sur Telemos
- Au 10x
![telflux10](./Flux/figures/Flux_TELEMOS_10x_14092023.png)

- Au 40x
![telflux40](./Flux/figures/Flux_TELEMOS_40x_14092023.png)

### Mars 2023

Avant Polypheme
![Polypheme flux](./Flux/figures/Flux_Polypheme_21032023.png)

Niveau de l'échantillon sur TELEMOS

Au 10x
![Polypheme flux](./Flux/figures/Flux_TELEMOS_10x_21032023.png)

Au 40x
![Polypheme flux](./Flux/figures/Flux_TELEMOS_40x_21032023.png)


